<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Assignment 2</title>
    
    <link id='wireframecss' type="text/css" rel="stylesheet" href="../wireframe.css" disabled>
    <link id='stylecss' type="text/css" rel="stylesheet" href="static/style.css?t=<?= filemtime("static/style.css"); ?>">
    <script src='../wireframe.js'></script>
  </head>

  <body>
  <nav>
    <header>
      <div class="top"><img id="logo" src='image/logo.png' />Lunardo Cinemas
      </div>
    </header>
    <ul>
      <li><a href="#now_showing" class="nav__link">Now Showing</a></li>
      <li><a href="#prices" class="nav__link">Prices</a></li>
      <li><a href="#about-us" class="nav__link">About Us</a></li>
    </ul>
</nav>


<div>
 <main>
  <div class="parallax1">
    <div id="now_showing">
     <div class="row">
          <article class='column'>
          <a href="#movie-AHF" onclick="myFunction('movie-AHF')"><img class='flim'  src="image/jojo.jpg" style="width: 223px;"></a>
              <div class="discription">
                  <h1 >JoJo Rabbit </h1><br><h1>PG/G: Koko: a Red Dog Story</h1>
                  <p>Mon-Tue : -<br>Wed-Fri : 12pm<br>Sat-Sun : 9pm</p>
              </div>
            </article>

            <article class='column'>
            <a href="#movie-ANM" onclick="myFunction('movie-ANM')"><img onclick="myFunction()" class='flim' src="image/prozen.jpg"></a>
            <div class="discription">
              <h1>Frozen 2 <br>PG/G: Frozen 2</h1>
              <p>Mon-Tue : -<br>Wed-Fri : 9pm<br>Sat-Sun : 6pm</p>

            </div>
            </article>

     </div>

    <div class="row">
      <article class='column'>
      <a href="#movie-RMC" onclick="myFunction('movie-RMC')"><img class='flim'  src="image/aero.jpeg"></a>
            <div class="discription">
               <h1>The Aeronauts <br>PG/G: The Queen’s Corgi</h1>
               <p>Mon-Tue : 6pm<br>Wed-Fri :-<br>Sat-Sun : 3pm</p>
              </div>
            </article>
              <article class='column'>
              <a href="#movie-ACT" onclick="myFunction('movie-ACT')"><img class='flim'  src="image/star-war.jpg"></a>
              <div class="discription">
               <h1>Star Wars: The Rise of Skywalker<br>PG/G: Playmobil: The Movie</h1>
               <p>Mon-Tue : 12pm<br>Wed-Fri : 6pm<br>Sat-Sun : 12pm</p>

              </div>
            </article>
       </div>
     </div> 
  </div>     
 </main>
</div>

<div class="parallax3">
    <div id="prices">
        <h2 align="center" style="background-color: aliceblue;">Ticket Prices</h2>
      <table>
        <tr>
            <th>Seat Type</th>
            <th>Seat Code</th>
            <th>All Day Monday & Tuesday AND 12pm on Wednesday , Thursday & Friday </th>
            <th>All Day Saturday & Sunday AND after 12pm on Wednesday ,Thursday& Friday</th>
          
        </tr>
        <tr>
            <td>Standard Adult</td>
            <td>STA</td>
            <td>15.00</td>
            <td>20.50</td>
          </tr>
        <tr>
          <td>Standard Concession</td>
          <td>STP</td>
          <td>13.00</td>
          <td>18.00</td>
        </tr>
        <tr>
          <td>Standard Child</td>
          <td>STC</td>
          <td>11.00</td>
          <td>15.50</td>
        </tr>
        <tr>
          <td>First Class Adult</td>
          <td>FCA</td>
          <td>25.00</td>
          <td>30.50</td>
        </tr>
        <tr>
          <td>First Class Concessionn</td>
          <td>FCP</td>
          <td>23.00</td>
          <td>27.50</td>
        </tr>
        <tr>
          <td>First Class Child</td>
          <td>FCC</td>
          <td>21.00</td>
          <td>25.50</td>
        </tr>
      </table>
    </div>
</div> 


<div id="about-us">
<h2 align="center" style="color: black; background-color: aliceblue;" >About Us</h2>
  <div class="parallax2">
      <h1 align="center">Big Movies is one of the best & Town First Multiplex operated by company Group.</h1>
      <br><h4 align="center">
      We have 3 Screens with a total seating capacity of 887 seats, providing world class auditoriums. 
      We are enabled by BARCO 2K Digital Projection System, 
      Crystal Clear DTS Sound System along with DOLBY ATMOS as an upg
      We currently have one screen with high quality 3D effect serving best sound and picture.<br></h4>
  <h1 align="center" style="color: red;">OUR MISSION: �TO OFFER A WORLD CLASS CINEMATIC EXPERIENCE.</h1>
  <h4 align="center">We strongly believe in bringing the highest exhibition standards to make your movie experience, �entertaining and memorable� with our services, starting from Ticket counter to EXIT.</h4>
      <div class="row">
            <div class="column">
              <img src="image/seat3.jpg" alt="Snow" style="width:100%">
              <h1 align="center">Standerd Seats</h1>
            </div>
            <div class="column">
              <img src="image/seat4.jpg" alt="Forest" style="width:100%">
              <h1 align="center">Premium Seats</h1>
            </div>
      </div>
  </div>
</div>

<p id="synopsis"></p>

<div id="booking_id">
      <center> <h1>Booking Area</h1></center>
      <h2><center id="day-movie"></center><center id="book-time"></center></h2>

     <center> <form id="contactform" name="form3" action="process.php" method="post" novalidate>
        <div class="fieldset">
        
          <fieldset>
              <legend>Standerd</legend>
              <label>Adults</label>
              <select id="seats[STA]" onchange="calculateTotal()" type="number" min="1" max="9" >
                  <option value="0">Please select</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>

              </select>
              <p><span id="errorMessage"></span></p>
              <label>Concession</label>
              <select id="seats[STP]" onchange="calculateTotal()" >
                  <option value="0">Please select</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>

              </select>
              <p><span id="errorMessage"></span></p>
              <label>Children</label>
              <select id="seats[STC]" onchange="calculateTotal()" >
                  <option value="0">Please select</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>

              </select>
              <br>
              <p><span id="errorMessage"></span></p>
          </fieldset>
          <fieldset>
          <legend>First Class</legend>
              <label>Adults</label>
              <select id="seats[FCA]" onchange="calculateTotal()" >
                  <option value="0">Please select</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>


              </select>
              <p><span id="errorMessage"></span></p>
              <label>Concession</label>
              <select id="seats[FCP]" onchange="calculateTotal()" >
                  <option value="0">Please select</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>

              </select>
              <p><span id="errorMessage"></span></p>
              <label for="title">Children</label>
              <select id="seats[FCC]" onchange="calculateTotal()">
                  <option value="0">Please select</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>

              </select>
              <br>
              <p><span id="errorMessage"></span></p>
          </fieldset>
        </div>
        <div class="fieldset">
          <fieldset class="fieldset1">
              <legend>Customer</legend> 

              <div class="input-row">
                <label for="name">Name </label>
                <input id="name" name="cust[name]" type="text" required>
                <i class="fa fa-check complete" aria-hidden="true"></i>
                <small id="alert-name"></small>
              </div>
    
              <div class="input-row">
                <label id="email-label" for="email">E-mail </label>
                <input id="email" name="cust[email]" type="email" required>
                <small id="alert-email"></small>
              </div>

              <div class="input-row">
                <label for="tel">Mobile </label>
                <input id="tel" name="cust[mobile] " type="tel" required>
                <small id="alert-tel"></small>
              </div>

              <div class="input-row">
                <label for="card">Credit Card  </label>
                <input id="card" name="cust[mobile]" type="text" required>
                <small id="alert-card"></small>
              </div>

              <div class="input-row">
                <label class="details__label" for="cust[expiryMonth]">Expiry</label>
                <input type="month" id="cust-expiry" name="cust[expiry]" class="details__text" onclick="setMinimumDate('cust-expiry')" required="true"></input>
                <small id='alert-expiry'></small>

              </div>
              <p id="total-price"></p>
              <button id="submit" name="submit" type="submit">Order</button>
              
          </fieldset>
          </div>
      
    <div id="display-message"></div>
 </form>
</center> 

</div>

    <footer>
      <div>&copy;<script>
        document.write(new Date().getFullYear());
      </script> Gaurav Adhikari s3833223. Last modified <?= date ("Y F d  H:i", filemtime($_SERVER['SCRIPT_FILENAME'])); ?>.</div>
      <div>Disclaimer: This website is not a real website and is being developed as part of a School of Science Web Programming course at RMIT University in Melbourne, Australia.</div>
      <div><button id='toggleWireframeCSS' onclick='toggleWireframe()'>Toggle Wireframe CSS</button></div>
    </footer>

  </body>
</html>
<script src="static/index.js"></script>
